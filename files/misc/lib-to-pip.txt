zlib-devel ---> liberasurecode-devel ---> PyECLib
postgresql-devel ---> psycopg2
libvirt-devel ---> libvirt-python
openldap-devel ---> python-ldap
libffi-devel ---> xattr

python-devel
patch
gettext-devel
yajl-devel
libxml2-devel
libpciaccess-devel
libnl-devel
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig/
pip install setuptools==1.4

for qpid-proton
    install cmake libuuid-devel
    unless ls /usr/include/proton/