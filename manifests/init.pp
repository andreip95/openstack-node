class openstack_node {
  include 'openstack_node::environment'
  include 'openstack_node::neutron'
  include 'openstack_node::nova'
}
