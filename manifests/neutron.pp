class openstack_node::neutron {
  package { 'openstack-neutron':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'openstack-neutron-linuxbridge':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'ebtables':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'ipset':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  file { "/etc/neutron/neutron.conf":
    mode => "0644",
    owner => 'neutron',
    group => 'neutron',
    source => 'puppet:///modules/openstack_node/configs/neutron/neutron.conf'
  }

  file { "/etc/neutron/plugins/ml2/linuxbridge_agent.ini":
    mode => "0644",
    owner => 'neutron',
    group => 'neutron',
    source => 'puppet:///modules/openstack_node/configs/neutron/linuxbridge_agent.ini'
  }

  service { 'neutron-linuxbridge-agent':
    ensure => 'running',
    enable => true
  }
}
