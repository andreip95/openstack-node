class openstack_node::nova {  
  package { 'openstack-nova-compute':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  package { 'sysfsutils':
    ensure => "installed",
    install_options => "--nogpgcheck"
  }

  file { "/etc/nova/nova.conf":
    mode => "0644",
    owner => 'nova',
    group => 'nova',
    content => epp('openstack_node/nova.conf.epp')
  }

  service { 'openstack-nova-compute.service':
    ensure => 'running',
    enable => true
  }

  service { 'libvirtd':
    ensure => 'running',
    enable => true
  }
}
